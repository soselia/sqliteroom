package com.example.sqlex

import androidx.room.*


@Dao
interface DAO {

    @Query("SELECT AVG(RunDistance)  FROM ${Table.TABLE_NAME}")
    fun getInfoArgOfRun(): Double?

    @Query("SELECT AVG(SwimDistance) FROM ${Table.TABLE_NAME}")
    fun getInfoArgOfSwim(): Double?

    @Query("SELECT AVG(Calories) FROM ${Table.TABLE_NAME}")
    fun getInfoArgOfCal(): Double?

    @Query("SELECT SUM(RunDistance) FROM ${Table.TABLE_NAME}")
    fun getInfoSumDistance(): Double?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg model: ModelClass)

    @Query("DELETE FROM ${Table.TABLE_NAME }")
    fun deleteall()
}