package com.example.sqlex

object Table {

    const val TABLE_NAME = "TableForEx"

    object TableColumns{
        const val ID = "ID"
        const val runDistance = "RDIST"
        const val swimDistance = "SDIST"
        const val calories = "CAL"
    }
}
