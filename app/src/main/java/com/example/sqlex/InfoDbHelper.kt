package com.example.sqlex

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.android.material.tabs.TabLayout

class InfoDbHelper(context: Context) : SQLiteOpenHelper(context,DbConfig.DATABASE_NAME,null,DbConfig.VERSION){

    companion object{
        private const val TABLE_CREATE = "CREATE TABLE ${Table.TABLE_NAME}(" +
                "${Table.TableColumns.ID} INTEGER PRIMARY KEY," +
                "${Table.TableColumns.swimDistance} REAL, " +
                "${Table.TableColumns.runDistance} REAL, " +
                "${Table.TableColumns.calories} REAL)"

        private const val DELETE_TABLE = "DROP TABLE IF EXISTS ${Table.TABLE_NAME}"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(TABLE_CREATE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, ov: Int, nw: Int) {
        db?.execSQL(DELETE_TABLE)
        onCreate(db)
    }

    fun insert(swimDistance: Double, runDistance: Double, calories: Double){
        val cv = ContentValues().apply {
            put(Table.TableColumns.swimDistance, swimDistance)
            put(Table.TableColumns.runDistance, runDistance)
            put(Table.TableColumns.calories, calories)
        }

        writableDatabase.insert(Table.TABLE_NAME, null, cv)
        Log.d("MyLog", "$swimDistance - $runDistance - $calories")
    }

    fun select(){

        val projection = arrayOf(
            Table.TableColumns.ID,
                Table.TableColumns.swimDistance,
                Table.TableColumns.calories,
                Table.TableColumns.runDistance
        )

        val cursor = readableDatabase.query(
                Table.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null
        )

        while (cursor.moveToNext()){
            val id = cursor.getInt(cursor.getColumnIndexOrThrow(Table.TableColumns.ID))
            val swim = cursor.getDouble(cursor.getColumnIndexOrThrow(Table.TableColumns.swimDistance))
            val run = cursor.getDouble(cursor.getColumnIndexOrThrow(Table.TableColumns.runDistance))
            val cal = cursor.getDouble(cursor.getColumnIndexOrThrow(Table.TableColumns.calories))

            Log.d("Selected", "$id - $swim - $run - $cal")
        }

        cursor.close()
    }

    fun deleteAll(){
        writableDatabase.delete(Table.TABLE_NAME,null,null)
    }

}