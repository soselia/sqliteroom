package com.example.sqlex

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import java.util.*

//შექმენით აპლიკაცია სადაც გექნებათ 3 input სადაც მომხმარებელმა უნდა შეიყვანოს :
//
//1.    გარბენილი მანძილი
//2.    გაცურული მანძილი
//3.    მიღებული კალორია
//
//შენახვის ღილაკზე დაჭერისას უნდა მოახდინოთ ამ ინფორმაციის შენახვა sqlite - ის გამოყენებით.
//
//ეკრანის ქვედა ნაწილში გამოიტანეთ :
//
//1.    საშუალო სტატისტიკა (საშუალო შედეგი სირბილსა და ცურვაში, ასევე ინფორმაცია საშუალოდ
// მიღებული კალორიების შესახებ)
//2.    ინფორმაცია ჯამურად გარბენილი მანძილის შესახებ
//
//გამოიყენეთ Room.
//
//პროექტი ატვირთეთ Public GIT Repository - ზე და გამოაგზავნეთ მხოლოდ Repository - ს ბმული.

class MainActivity : AppCompatActivity() {

    private lateinit var swimDistText: EditText
    private lateinit var runDistText: EditText
    private lateinit var calDistText: EditText
    private lateinit var avrRunDistance: TextView
    private lateinit var avrCal: TextView
    private lateinit var avrSwimDistance: TextView
    private lateinit var sumOfDist: TextView
    private lateinit var button: Button

    private val infoDb = InfoDbHelper(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.hide()

        swimDistText = findViewById<EditText>(R.id.swimming)
        runDistText = findViewById<EditText>(R.id.running)
        calDistText = findViewById<EditText>(R.id.calories)
        avrRunDistance = findViewById(R.id.textView6)
        avrSwimDistance = findViewById(R.id.textView5)
        avrCal = findViewById(R.id.textView7)
        sumOfDist = findViewById(R.id.textView8)
        button = findViewById<Button>(R.id.button)

        button.setOnClickListener{
            if (!swimDistText.text.isNullOrEmpty() && !runDistText.text.isNullOrEmpty() && !calDistText.text.isNullOrEmpty()){
                insertInfoSQLite()
                insertInfoRoom()
                clearEditText()
            }
            getRunAVG()
            getSwimAVG()
            getCalAVG()
            sumOfDistanceFromRoom()
        }

        infoDb.select()
//       infoDb.deleteAll()
//        App.instance.db.getDaoInfo().deleteall()
    }

    fun insertInfoSQLite(){
        val swimDist: Double = swimDistText.text.toString().toDouble()
        val runDist:Double = runDistText.text.toString().toDouble()
        val calories:Double = calDistText.text.toString().toDouble()

        infoDb.insert(swimDist,runDist, calories)
    }

    fun insertInfoRoom(){
        val swimDist: Double = swimDistText.text.toString().toDouble()
        val runDist:Double = runDistText.text.toString().toDouble()
        val calories:Double = calDistText.text.toString().toDouble()

        App.instance.db.getDaoInfo().insert(ModelClass(0,runDist,swimDist,calories))
    }

    fun getRunAVG(){
        avrRunDistance.text = App.instance.db.getDaoInfo().getInfoArgOfRun().toString()
    }

    fun getSwimAVG(){
        avrSwimDistance.text = App.instance.db.getDaoInfo().getInfoArgOfSwim().toString()
    }

    fun getCalAVG(){
        avrCal.text =  App.instance.db.getDaoInfo().getInfoArgOfCal().toString()
    }

    fun sumOfDistanceFromRoom(){
        sumOfDist.text = App.instance.db.getDaoInfo().getInfoSumDistance().toString()
    }

    fun clearEditText(){
        swimDistText.text.clear()
        runDistText.text.clear()
        calDistText.text.clear()
    }


}