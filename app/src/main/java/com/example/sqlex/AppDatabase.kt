package com.example.sqlex

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [ModelClass::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getDaoInfo(): DAO

}