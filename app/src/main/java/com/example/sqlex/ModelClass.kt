package com.example.sqlex

import android.icu.text.IDNA
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = Table.TABLE_NAME)
data class ModelClass(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "ID")
    val id: Int,

    @ColumnInfo(name = "RunDistance")
    val run: Double,

    @ColumnInfo(name = "SwimDistance")
    val swim: Double,

    @ColumnInfo(name = "Calories")
    val cal: Double
)
